# no perl path on top - cmd line executable only

# ---------------------------------------------
# filename: build.pl
# author:   Frode Klevstul (frode@klevstul.com)
# started:  22.12.2007
# version:	v01_20071222
# ---------------------------------------------



# -------------------
# use
# -------------------
use strict;
use lib '\\\\s083\shared\klevstulProductions\0705_kComIntegrated\svn\trunk\kCom.i\cgi';
use kCom;


# -------------------
# declarations
# -------------------
my $targetDir		= '\\\\s083\shared\klevstulProductions\0705_kComIntegrated\kCom.i\cgi';				# use the full path to the target directory (where kCom.i's cgi files are stored) to avoid building the source code (tips for kCom.i developers only)
my $cgiExtension	= 'cgi';
my $buildIniFile	= '\\\\s083\shared\klevstulProductions\0705_kComIntegrated\svn\trunk\kCom.i\build.ini';



# -------------------
# main
# -------------------
print qq(\nbuilding kCom.i\n\n);
&build_fixCgiFiles;



# -------------------
# sub
# -------------------
sub build_fixCgiFiles{
	my $file;
	my $input;
	my $filename;
	my $extension;
	my $line;
	my @parameter;
	my $parameter;
	my @value;
	my $i;
	my @fileContent;

	# ---
	# get all paramters with values from the build initialisation file
	# ---
    open(BUILD_INI, "<$buildIniFile") || &kCom_error("build_fixCgiFiles","failed to open '$buildIniFile'");
	LINE: while ($line = <BUILD_INI>){
		if ($line =~ m/^#/){
			next LINE;
		} elsif ($line =~ m/^(\w+)(\t)+(.*)$/){
			push(@parameter,$1);
			push(@value,$3);
		}
	}
	close (BUILD_INI);

	# ---
	# get names of all files in the directory the cgi files are stored
	# ---
	opendir (D, "$targetDir");
	my @files = grep /\w/, readdir(D);
	closedir(D);

	# go through all filenames
	foreach $file (sort @files){
		if ($file =~ m/(\w+)\.(\w+)$/){
			$filename	= $1;
			$extension	= $2;
			
			if ($extension !~ m/$cgiExtension/){
				next;
			}

			# open all cgi files
		    open(CGI_FILE, "<$targetDir/$file") || &kCom_error("build_fixCgiFiles","failed to open '$targetDir/$file' for reading");
			LINE: while ($line = <CGI_FILE>){
				
				# replace all <�TAGS�> with values from build file
				$i = 0;
				foreach $parameter (@parameter){
					$line =~ s/<�-$parameter-�>/$value[$i]/sgi;
					$i++;
				}
				# add altered line to the parameter storing all the file's data
				push(@fileContent,$line);
			}
			close (CGI_FILE);


			# overwrite the cgi file with the new content
			open (CGI_FILE, ">$targetDir/$file") || &kCom_error("build_fixCgiFiles","failed to open '$targetDir/$file' for writing");
			flock (CGI_FILE, 2);																	# lock file for writing
			foreach (@fileContent){
				print CGI_FILE $_;
			}
			close (CGI_FILE);
			flock (CGI_FILE, 8);																	# unlock file
			@fileContent = undef;																		# reset the file content
		}
	}
}
