#!<�-PERL_PATH-�>

# ---------------------------------------------
# filename:	kIndex.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	23.10.2002
# build:  	<�-BUILD_VERSION-�>
# version:	v02_20071223
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){							# $^O = Current Server Operating System; linux | MSWin32
    use lib '<�-LINUX_LIBRARY-�>';
}
use kCom;



# ------------------
# declarations
# ------------------



# ------------------
# main
# ------------------
&kCom_printTop("kIndex");
&printMain;
&kCom_printBottom;



# ------------------
# sub
# ------------------

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		prints out layout and calls plugins on index page
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub printMain{
	my @layout;
	my $layout;
	my @modules;
	my $module;

	my $mod;
	my $param;
	my $html;

	if (&kCom_login){
		@layout = &kCom_getIniValue('kIndex','LAYOUT');
	} else {
		@layout = &kCom_getIniValue('kIndex','LAYOUT_NOTLOGGEDIN');	
	}

	# before split -
	# 	orginal line: 	"<tr><td>[m1(1)]</td><td>[m2(5)]</td><td>[m3(1)]</td></tr>"
	# after split - 
	# 	$modules[0]:		"<tr><td>"
	# 	$modules[1]: 		"m1(1)]</td><td>"
	# 	$modules[2]: 		"m2(5)]</td><td>"
	# 	$modules[3]: 		"m3(1)]</td></tr>"

	foreach $layout (@layout){

		if ($layout =~ m/URL:\s?(.*)/){
			&kCom_jumpTo($1);
		} else {
			@modules = split(/\[/, $layout);
			
			foreach $module (@modules) {
				$mod = undef;
				$param = undef;
				if ($module =~ m/^m(\d+)\((\d+)\)\](.*)/){
					$mod = $1;
					$param = $2;
					$html = $3;
				} elsif ($module =~ m/^m(\d+)\](.*)/){
					$mod = $1;			
					$html = $2;
				} else {
					$html = $module;
				}
				if ($mod != undef){
					if ($mod == 11){
						&kCom_kIndexPlugin_contactInfo;
					} elsif ($mod == 12){
						&kCom_kIndexPlugin_news($param);
					} elsif ($mod == 20){
						&kCom_kFeeder_getLatestComments($param);
					} elsif ($mod == 21){
						&kCom_kFeeder_getMostPop($param);
					}				
				}
				print "$html\n";
			}
		}
	}
}
