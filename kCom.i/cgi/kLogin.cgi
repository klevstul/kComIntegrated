#!<�-PERL_PATH-�>

# ---------------------------------------------
# filename:	kLogin.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	24.10.2004
# build:  	<�-BUILD_VERSION-�>
# version:	v02_20071223
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use strict;
use CGI;
if ($^O eq "linux"){
    use lib '<�-LINUX_LIBRARY-�>';
}
use kCom;



# ------------------
# declarations
# ------------------
my $query			= new CGI;
my $p_password		= $query->param('p_password');
my $p_url			= $query->param('p_url');
my $p_action		= $query->param('p_action');

my @password		= &kCom_getIniValue("kLogin","PASSWORD");
my @domain			= &kCom_getIniValue("kLogin","DOMAIN");
my @cookie			= &kCom_getIniValue("kLogin","COOKIE");

my $exp_date = 0;
my $domain = $domain[0];
my $path = "/";



# ------------------
# main
# ------------------
if ($p_action eq "logout") {
	&kLogin_logout;
} elsif ($p_action eq "login") {
	&kLogin_login;
} else {
	&kLogin_form;
}



# ------------------
# sub
# ------------------
sub kLogin_login{

	#my $date = time;

	if ($p_url eq undef) {
		$p_url = "kIndex.cgi";
	}

	if ($p_password eq $password[0]){
		&kCom_setCookie($cookie[0], "loggedIn",  $exp_date, $path, $domain);	
	}

	if ($p_password eq $password[0]){
		&kCom_printTop;
		print("<br><br>Logging in!<br>");
		&kCom_jumpTo($p_url, 0, 3);
		&kCom_printBottom;
	} else {
		&kCom_printTop;
		&kCom_printError("kLogin","Wrong password, access denied.");
		&kCom_jumpTo($p_url, 0, 3);
		&kCom_printBottom;
	}
}



sub kLogin_logout{

	&kCom_setCookie($cookie[0], "loggedIn",  0.001, $path, $domain);	

	&kCom_printTop;
	print("<br><br>Logging out!<br>");
	&kCom_jumpTo("kIndex.cgi", 0, 3);
	&kCom_printBottom;
}



sub kLogin_form{

	&kCom_printTop;
	&kCom_tableStart("login");
	print qq(
		<form action="kLogin.cgi" method="post" name="kLogin">
		<input type="hidden" name="p_action" value="login">
		<input type="hidden" name="p_url" value="$p_url">
		password: <input type="password" name="p_password" size="5" maxlength="20">
		<a href="javascript:document.kLogin.submit()">login</a>
		</form>
	);
	&kCom_tableStop;
	&kCom_printBottom;
}
