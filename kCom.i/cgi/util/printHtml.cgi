#!/usr/bin/perl
#!C:/prgFiles/perl/bin/Perl.exe

# ---------------------------------------------
# filename:	printHtml.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	10.12.2004
# version:	v01_20041210
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
use CGI;
my $base;
if ($^O eq "linux"){
	$base = '/home/klevstul/www/watashi/kCom.i/cgi/';
	use lib '/home/klevstul/www/watashi/kCom.i/cgi/';
	chdir("/home/klevstul/www/watashi/kCom.i/cgi");
} else {
	$base = 'C:/klevstul/private/projects/kCom/cgi/';
	use lib 'C:/klevstul/private/projects/kCom/cgi/';
	chdir("C:/klevstul/private/projects/kCom/cgi/");	# have to change directory to get ini files right
}
use kCom;


# ------------------
# declarations
# ------------------
my $query = new CGI;
my $type = $query->param('type') || "top";
my $urlbase = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}"; 
$urlbase =~ s/extra\/.*\.cgi.*//;


# ------------------
# main
# ------------------
if ($type eq "top") {
	&kCom_printTop("", $urlbase);
} elsif ($type eq "bottom"){
	&kCom_printBottom;
} else {
	print "Content-type: text/html\n\n"; 
	&kCom_printError("printHtml.cgi", "Unknown parameters, please use 'type=[top|bottom]");
}



