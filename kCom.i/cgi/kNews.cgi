#!<�-PERL_PATH-�>

# ---------------------------------------------
# filename:	kNews.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	01.11.2003
# build:  	<�-BUILD_VERSION-�>
# version:	v02_20071223
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
    use lib '<�-LINUX_LIBRARY-�>';
}
use kCom;
use CGI;



# ------------------
# declarations
# ------------------
my $query			= new CGI;
my $message			= $query->param('message');
my $link			= $query->param('link');
my $pwd				= $query->param('p');
my @password		= &kCom_getIniValue("kNews","NEWS_PASSWORD");



# ------------------
# main
# ------------------
&kCom_printTop('kNews');
if ($message ne undef && $pwd eq $password[0]){
	&kNews_add;
} elsif ($pwd eq $password[0]) {
	&kNews_form;
}
&kCom_printBottom;



# ------------------
# sub
# ------------------
sub kNews_add{

	my @newsfile 	= &kCom_getIniValue("kNews","PATH_NEWSFILE");
	my $timestamp	= &kCom_timestamp();
	my $host_ip		= &kCom_getHostIP;

	if ($link ne undef){
		$message = "<a href=\"$link\">$message</a>";
	}

	open (FILE, "<$newsfile[0]") || &kCom_error("kNews_add","failed to open '$newsfile[0]'");
	flock (FILE, 1);		# share reading, don't allow writing
	my $content = join("",(<FILE>));
	close (FILE);
	flock (FILE, 8);		# unlock file

	open (FILE, ">$newsfile[0]") || &kCom_error("kNews_add","failed to open '$newsfile[0]'");
	flock (FILE, 2);		# lock file for writing
	print FILE "$timestamp���$host_ip���$message\n" . $content;
	close (FILE);
	flock (FILE, 8);		# unlock file

	# jumps back to frontpage
	&kCom_jumpTo("kIndex.cgi",0,2);
}



sub kNews_form{

	&kCom_tableStart("Add news","95%");
	print qq(
		<form method="POST" name="form" action="kNews.cgi">
		<input type="hidden" name="p" value="$pwd">
		<table>
		<tr>
			<td>message:</td>
			<td><input type="text" name="message" size="40" maxlength="35">
		</tr>
		<tr>
			<td>link:</td>
			<td><input type="text" name="link" size="50" maxlength="150">
		</tr>
		<tr>
			<td colspan="2" align="right"><a href="javascript:document.form.submit()">add news</a></td>
		</tr>
		</table>
		</form>
	);
	&kCom_tableStop;
}
