#!<�-PERL_PATH-�>

# ---------------------------------------------
# filename: kFeeder.cgi
# author:   Frode Klevstul (frode@klevstul.com)
# started:  20.05.2007
# build:  	<�-BUILD_VERSION-�>
# version:	v02_20080809
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use strict;
use LWP::Simple;   
use HTML::Entities;
use CGI;
if ($^O eq "linux"){
    use lib '<�-LINUX_LIBRARY-�>';
    use Encode;
} else {
    use Unicode::String;
}
use kCom;



# ------------------
# declarations
# ------------------
my $debug			= 0;
my $query			= new CGI;
my $p_feeder		= $query->param('p_feeder');
my $p_action		= $query->param('p_action');
my $p_url			= $query->param('p_url');
my $p_tag			= $query->param('p_tag');
my $feederProblem;
my @feedURL       	= &kCom_getIniValue($p_feeder,"ATOM_FEED", 1, 0, 1);
my @path_comments;
my @editURL;
my @topLinks;
my @feederAuthorLine;
my @showTags;
my @commentTags;
my @tagBase;
my @max_postings;
my @blog_base_url;
my @allow_comments;
my @blog_id;
my @list_type;

if ($feedURL[0] eq ""){
	$feederProblem = 1;
} else {
	# global feeder settings
	@path_comments		= &kCom_getIniValue("kFeeder","PATH_COMMENTS");
    @editURL			= &kCom_getIniValue("kFeeder","EDIT_URL");
    @topLinks			= &kCom_getIniValue("kFeeder","FEEDER_TOPLINKS");
    @feederAuthorLine	= &kCom_getIniValue("kFeeder","FEEDERAUTHORLINE");
    @showTags			= &kCom_getIniValue("kFeeder","SHOW_TAGS");
    @commentTags		= &kCom_getIniValue("kFeeder","COMMENT_TAGS");

	# local feeder settings
	@tagBase			= &kCom_getIniValue($p_feeder,"ATOM_FEED_TAGBASE");
	@max_postings		= &kCom_getIniValue($p_feeder,"MAX_POSTINGS");
	@blog_base_url		= &kCom_getIniValue($p_feeder,"BLOG_BASE_URL");
	@allow_comments		= &kCom_getIniValue($p_feeder,"ALLOW_COMMENTS");
	@blog_id			= &kCom_getIniValue($p_feeder,"BLOG_ID");
	@list_type			= &kCom_getIniValue($p_feeder,"LIST_TYPE");
}


# ------------------
# main
# ------------------
&kCom_printTop("kFeeder");
if ($feederProblem){
	&kCom_printError('kFeeder', 'Could not read value ATOM_FEED from \''.$p_feeder.'.ini\' (file might be missing)');
} else	{
	if ($p_action eq "show") {
	    &kFeeder_getBlogContent($p_url);
	} elsif ($p_action eq "com") {
	    &kFeeder_addComment($p_url);
	}
}
&kCom_printBottom;



# ------------------
# sub
# ------------------
sub kFeeder_getBlogContent {
    my $url = $_[0];

    my $webObject;
    my @entries;
    my $title;
    my $published;
    my $content;
    my $post_link;
    my $author;
    my $counter;
    my $show_single;
    my $alternate_link;
    my $post_id = $url;
    $post_id =~ s/.*\/(.*$)/$1/;
	my @tags;
	my $tag;
	my @tmp;
	my $tmp;
	my $overridAllowComment;

	# if we've received a tag we're going to list all postings with that tag.
	# to do this we use the $url value as if we were going to list one entry only
	if ($p_tag ne undef){
		$url			= $p_tag;
		$show_single	= 0;
	# if no tag it's single view only we we're given a url
	} else {
	    # if the url is empty it means we're going to list out many postings
	    if ($url ne undef){
	        $show_single = 1;
	    } else {
	        $show_single = 0;
	    }
	}

	# print out debugging information
	if ($debug){
		print qq(
			TAG: $p_tag <br>
			BLOG ID BASE URL: $blog_base_url[0]$url <br>
			TAG BASE URL: $tagBase[0]/$url <br>
		);
	}

    # download the web object
    if ($show_single == 0 && $p_tag eq undef){
        $webObject = &get($feedURL[0]);
    } elsif ($p_tag eq undef) {
        $webObject = &get($blog_base_url[0].$url);
	# in the last case we're given a tag
	} else {
        $webObject = &get($tagBase[0]."/".$url);
    }

    # the feed is returned to us as UTF-8 so we have to decode it into latin characters
    if ($^O ne "linux"){
		$webObject = Unicode::String::utf8($webObject)->latin1();
    } else {
		Encode::from_to($webObject, "utf8", "iso-8859-1");
    }

    # check if the feed is proper Atom feed
    if ($webObject !~ m/xmlns='http:\/\/www.w3.org\/2005\/Atom'/sgi){
        &kCom_printError('kFeeder', 'Not recognised Atom Feed');
    }

    # First decode takes care of '<' and '>'
	$webObject = &kFeeder_initialDecode($webObject);

    # strip away everything before the first <entry> tag
    $webObject =~ s/^.*?(<entry>.*$)/$1/sgi;

    # strip away everything after the last </entry> tag
    $webObject =~ s/(^.*<\/entry>).*$/$1/sgi;

    # split the entries and push it onto an array
    @entries = split('</entry>', $webObject);

	# print kFeeder's top links
	print qq(<table width="100%"><tr><td>);
	foreach (@topLinks){
		print qq($_\n);
	}
	print qq(<tr><td>&nbsp;</td></tr></td></tr></table>);

    # go through the entries
    FOREACH: foreach (@entries){
        $counter++;
        $overridAllowComment = undef;

        # ---
        # select out different XML formatted values
        # ---
        if ($_ =~ m/<published.*?>(.*)<\/published>/si){
            $published = $1;
            $published =~ s/(.*)T\d{2}:.*/$1/sgi;
        }

        if ($_ =~ m/<title.*?>(.*)<\/title>/si){
            $title = $1;
        }

        if ($_ =~ m/<content.*?>(.*)<\/content>/si){
            $content = $1;
            $content =~ s/<a href/ <a target="blank_" href/sgi;
        }

        if ($_ =~ m/<link rel='self' type='application\/atom\+xml' href='(.*?)'\/>/si){
            $post_link = $1;
            $post_link =~ s/$blog_base_url[0]//is;
        }

        if ($_ =~ m/<link rel='alternate' type='text\/html' href='(.*?)' title='/si){
            $alternate_link = $1;
        }

        if ($_ =~ m/<author.*?>(.*)<\/author>/si){
            $author = $1;
            if ($author =~ m/<name.*?>(.*)<\/name>/si){
                $author = $1;
            }
        }

		# get all tags from the content we're listing out
        if ($_ =~ m/<category\s/si){
		    @tmp = split('<category\s', $_);
			foreach $tmp (@tmp){
		        if ($tmp =~ m/\sterm='(.+?)'\/>/si){
		        	$tag = $1;

					# if this posting's tag / label matches "comment tag" we'll override and set comment to yes
	            	if ($commentTags[0] =~ m/.*$tag.*/s){
	            		$allow_comments[0] = 'yes';
	            	}

		            foreach (@tags){
		            	if ($_ eq $tag){
		            		$tmp = 'stored';
		            	}
		            }

		            if ($tmp ne 'stored'){
			            push(@tags, $tag);
					}
				}
			}
        }

        # ---
        # print HTML
        # ---
        if ($list_type[0] eq "full" or $show_single == 1){

		    &kCom_tableStart($title);
			print qq(
				<table width="100%">
	            <tr><td><div>
	            	$content
	            </div></td></tr>
	            <tr><td>&nbsp;</td></tr>
	        );

	        if ($show_single == 1 && $feederAuthorLine[0] ne "hide") {

		        print qq(
	    	        <tr class="feederAuthorLine"><td class="smallText">
				);
				if ($feederAuthorLine[0] eq "full") {
					print qq(
						[ $author // $published
						// <a class="discreetAuthorLineLink" href="$editURL[0]?blogID=$blog_id[0]&amp;postID=$post_id" target="blank">edit</a>
						// <a class="discreetAuthorLineLink" href="$alternate_link" target="blank">external</a>
						]
					);
				} elsif ($feederAuthorLine[0] eq "anonymous") {
					print qq(
						[ $published
						// <a class="discreetAuthorLineLink" href="$editURL[0]?blogID=$blog_id[0]&amp;postID=$post_id" target="blank">edit</a>
						// <a class="discreetAuthorLineLink" href="$alternate_link" target="blank">external</a>
						]
					);
				} elsif ($feederAuthorLine[0] eq "minimal") {
					print qq(
						<a class="discreetAuthorLineLink" href="$editURL[0]?blogID=$blog_id[0]&amp;postID=$post_id" target="blank">.</a>
					);
				}
		        print qq(
	    	        </td></tr>
				);

			} else {
		        print qq(
	    	        <tr class="feederAuthorLine">
	    	        	<td class="smallText">
	    	        	[ $author // $published
	    	        	// <a href="kFeeder.cgi?p_feeder=$p_feeder&amp;p_action=show&amp;p_url=$post_link">single view</a>
	    	        	]
	    	        	</td>
	    	        </tr>
				);
			}

	        if ($show_single == 1 && $allow_comments[0] eq "yes") {
	            print qq(
	                <tr><td align="right">[<a href="kFeeder.cgi?p_action=com&p_feeder=$p_feeder&p_url=$url">add comment</a>]</td></tr>
	            );
	            if (-e "$path_comments[0]/$post_id\.com"){
	                print qq(<tr><td>);
	                &kCom_listComments("$path_comments[0]/$post_id\.com");
	                print qq(</td></tr>);
	            }
	        }

			print qq(</table>);
		    &kCom_tableStop;
		    print qq(<br>);

		} elsif ($list_type[0] eq "title") {

			print qq(<table width="100%">);

			if ($counter%2 == 0){
				print qq(<tr class="alternateLine">);
			} else {
				print qq(<tr>);
			}

	        print qq(
	        	<td width="80%"><a href="kFeeder.cgi?p_feeder=$p_feeder&amp;p_action=show&amp;p_url=$post_link">$title</a></td>
	        	<td class="smallText">
			);

			if ($feederAuthorLine[0] eq "full"){
				print qq([$author // $published]\n);
			} elsif ($feederAuthorLine[0] eq "anonymous"){
				print qq([$published]\n);
			} else {
				print qq(&nbsp;\n);
			}

	        print qq(</td></tr></table>\n);
		}

        # stop looping when we've found enough postings
        if ($counter == $max_postings[0]){
            last FOREACH;
        }
    }

	# print out tags alphabetically (if there are any tags)
	if (scalar @tags > 0 && $showTags[0] eq "true"){
		print qq(<br><table><tr class="tagLine">);
		foreach (sort @tags){
			print qq(<td><a href="kFeeder.cgi?p_feeder=$p_feeder&amp;p_action=$p_action&amp;p_tag=$_">$_</a></td>);
		}
		print qq(</table>);
	}
}



sub kFeeder_addComment{
    my $url = $_[0];
    my $post_id = $url;
    $post_id =~ s/.*\/(.*$)/$1/;
    my $return_url = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}?p_action=show&amp;p_feeder=$p_feeder&amp;p_url=$url";
    &kCom_addComment("$path_comments[0]/$post_id\.com",undef,$return_url);
}



sub kFeeder_initialDecode {
	my $content = $_[0];
	$content =~ s/&gt;/>/sg;
	$content =~ s/&lt;/</sg;
	$content =~ s/&amp;/&/sg;
	return $content;
}
