#!<�-PERL_PATH-�>

# ---------------------------------------------
# filename:	kComment.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	05.03.2003
# build:  	<�-BUILD_VERSION-�>
# version:	v02_20071223
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
    use lib '<�-LINUX_LIBRARY-�>';
}
use kCom;
use CGI;



# ------------------
# declarations
# ------------------
my $query			= new CGI;
my $comment_file	= $query->param('comment_file');
my $return_url		= $query->param('return_url');
my $email			= $query->param('email');
my $comment			= $query->param('comment');
my $vcode			= $query->param('vcode');



# ------------------
# main
# ------------------
if ($comment ne undef){
	if ((&kCom_verifyCode($vcode) == 1) || ($^O ne "linux")){										# on Windows we can't verify the code so we allow comments
		&kComment_registerComment;
	}
}
&kCom_jumpTo($return_url,1);



# ------------------
# sub
# ------------------
sub kComment_registerComment{
	my $timestamp	= &kCom_timestamp();
	my $host_ip		= &kCom_getHostIP;

	open (FILE, "<$comment_file");
	flock (FILE, 1);																				# share reading, don't allow writing
	my $content = join("",(<FILE>));
	close (FILE);
	flock (FILE, 8);																				# unlock file

	if ($content ne "" && $content !~ m/\n$/sg){													# have to do this check since new comments in the first place were written on top, but now on the bottom
		$content .= "\n";
	}

	if ($content eq ""){
		$content .= "RETURN_URL\t$return_url\n\n";
	}

	open (FILE, ">$comment_file") || &kCom_error("kComment_registerComment","failed to open '$comment_file'");
	flock (FILE, 2);																				# lock file for writing	
	print FILE $content . "$timestamp���$host_ip���$email���$comment\n";
	close (FILE);
	flock (FILE, 8);																				# unlock file

}
